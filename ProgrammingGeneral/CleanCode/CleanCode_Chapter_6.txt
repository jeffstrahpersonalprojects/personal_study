Objects and Data Structures

	Try to use abstract implementations.
		Don't let the user know everything going on in a class, instead figure out what the user of the class will need, and expose only those. 
		Don't let the user manipulate things they have no reason/purpose to manipulate. Don't get/set everything if it only confuses the system.

	OO style programming relies on including methods and functions on the data structure, which requires continual maintenance on the objects themselves, requireing editing and maintenance whenever any new functionality or method is required.

	Procedural style programming relies on including the the methods or funcitons on a class that operates on data structures, meaning that everytime a data structure is added or edited that the class must be edited. 

	Law of Demeter: A module should not know about the innards of the object it manipulates.
		Method F of class C should only call methods related to:
			C
			An object created by F
			An object passed as an argument to F
			An object held as an instance variable of C
		Law of Demeter applies to Objects, but not Data Structures, since Data Structures are exposed by their very nature

	Try to create either an object, or a data structure, do not mix the two.

	Objects
		Expose Behavior
		Hide Data
	Data Structure
		Expose Data
		Behavior agnostic