Formatting:

	Vertical:
	Maintain vertical spacing between concept blocks. 
	Provide spacing between groupings or concepts. Include for example a line below global variables, and lines between methods. 
	Group together things like global vairables, or related objects/methods
	Always declare variables related to loop
	Keep instance declarations at the top of the class
	Methods should flow in a downward manner. Each function/method should call to one below it.

	Horizontal:
	Try to keep a line to 120 characters wide at most. 