Comments: 
	
	Avoid comments whenever possible.
	Comments become outdated and provide bad information very quickly. 

	Comments to use:
		Legal and author comments at top of document. 
		Information related to esoteric methods or variables
		Explanation of intent, why the method does what it does. Especially with coded or obscure return types
		Calrification for things like tests, where effects of particular methods or classes might not be readily evident
		Warning of consequences from hitting a particular method 
		TODO comments, but don't use it as an excuse to be lazy
		Note EXTREMELY important things
		Documentation for API

	Comments to avoid:
		Unclear comments, either as to intent or intended audience
		Redundant comments 
