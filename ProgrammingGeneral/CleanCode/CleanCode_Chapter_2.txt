Names:

	Use Intention-Revealing Names 
		try to make sure any variable or method's name tells what it is, what it does, and how it behaves. 

	Avoid words with multiple meanings: 
		Don't use the word 'List' in a variable name unless it's ACTUALLY a list
		
	Keep consistent in naming conventions. 
	
	Make sure differences are easily spottable 

	Make variable and method names human readable

	Use readable and searchable names. If possible give a constant a variable name, that way when searching for it, you're not looking for a simple number, but rather a concept name. Also searching for numbers can be a pointless endeavor 

	Class names should be noun based. Avoid phrases like Manager, Processor, Data, or Info. 

	Method names, when applicable should reflect their main function. i.e.: getName(), setName(), ifNameIsNull().