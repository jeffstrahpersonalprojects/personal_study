;;This creates a basic number guesser between 1-100. It does a binary search.
;; the function guess-my-number makes a guess, then the functions bigger and smaller are used to
;; state if your guess is bigger or smaller than the nuber provided. Once the program guesses
;; correctly start-over begins again at 50
(defparameter *small* 1)
(defparameter *big* 100)
(defun guess-my-number ()
	(ash (+ *small* *big*) -1))
(defun smaller()
	(setf *big*(1- (guess-my-number)))
	(guess-my-number))
(defun bigger()
	(setf *small* (1+ (guess-my-number)))
	(guess-my-number))
(defun start-over()
	(defparameter *small* 1)
	(defparameter *big* 100)
	(guess-my-number))