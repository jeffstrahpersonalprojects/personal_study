;;This creates a function my-length, which takes in a list (Remember to note it as data not code) 
;; It then counts the number of elements in the list and returns that number
(defun my-length (list)
	(if list
		(1+ (my-length (cdr list)))
		0))