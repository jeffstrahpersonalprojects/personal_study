(defparameter *allowed-commands* '(look walk pickup inventory drop))

(defun game-repl()
	(let ((user-command (game-read)))
		(unless (eq (car user-command) 'quit) ;;We check to see if the user wants to quit. If it doesn't
			(game-print (game-eval user-command)) ;;We send the user input to be evaluated, then print it
			(game-repl)))) ;;we recursively loop the repl until the user inputs quit

(defun game-read()
	(let 
		((user-command (read-from-string ;;We start getting the user command by reading from string, the users direct literal input, then adding parentheses so we may use it as code
			(concatenate 'string "(" (read-line) ")" )))) ;;We append parentheses so the user command can be read as code, and set it as a string
	(flet ((quote-it(input-to-quote) ;;We create a local function to quote the incoming input
			(list 'quote input-to-quote))) ;;We create a list with a quote on the front so that we can use it as code 
		(cons (car user-command) (mapcar #'quote-it (cdr user-command)))))) 

(defun game-eval (user-command) 
	(if (member (car user-command) *allowed-commands*) ;;If the user-command is one of the defined allowed commands
		(eval user-command) ;;If it is, then we evaluate the input
	'(I do not know that command.))) ;;If not, then we let the user know we don't like it.

(defun tweak-text (user-input capitalize-this-character use-literal-value)
	(when user-input ;;;;When we have user input 
		(let (
			(item (car user-input)) ;;; Takes the first character and operates on that 
			(rest (cdr user-input))) ;;; stores the rest to be passed in to the next iteration of a function
		(cond 	((eq item #\space)
					(cons item (tweak-text rest capitalize-this-character use-literal-value))) ;; If character is a space, do not operate on, and pass remaining characters back into list, do not change capitalization rule for next character
				((member item '(#\! #\? #\.))
					(cons item (tweak-text rest t use-literal-value))) ;; If character is a ! ? or . do not operate on, and pass remaining characters back into list, and mark next character for capitalization
				((eq item #\") 
					(tweak-text rest capitalize-this-character (not use-literal-value))) ;; If character is a quote. If it is, we switch on the LITERAL value (or off if it's an end quote) so that we can read in capitalization and punctuation correctly. We maintain capitalization rules.
				(use-literal-value 
					(cons item (tweak-text rest nil use-literal-value))) ;; If we are supposed to use literal values, then we use the damn literal value. But we no longer let capitalization affect printing
				((or capitalize-this-character use-literal-value) 
					(cons (char-upcase item) (tweak-text rest nil use-literal-value)));;If we are supposed to upper case, but not print literal, then we make the character uppercase, and pass in the rest of the list
				(t 
					(cons (char-downcase item) (tweak-text rest nil nil ))))))) ;; otherwise we pass in a lower case character

(defun game-print (user-input)
	(princ (coerce ;;we take in a list of characters, and princ it as a string 
		(tweak-text ;;we generate the text to be princ'd in the following steps
			(coerce (string-trim "() " ;;we remove the parenteheses and turn it into a list of chars
				(prin1-to-string user-input)) ;; we convert the user input into a string
				'list) ;;we take the user input
			t
			nil) ;;We send to tweak text the list of user input, the command to capitalize the first character, and the flag to not read it literally
			'string)) ;; we then coerce it back into a string 
		(fresh-line)) ;;finally we carriage return 