;;PuddingEaterCase - same as the previous example, only using Case instead of cond
(defvar *arch-enemy* nil)

(defun pudding-eater (person) 
	(case person
		((henry) (setf *arch-enemy* 'stupid-lisp-alien)
				'(curse you lisp aline - you ate my pudding))
		((johnny) (setf *arch-enemy* 'useless-old-johnny)
				'(i hope you choked on my pudding johnny))
		(otherwise '(why you eat my pudding stanger ? ))))
