Creating a text-based game
	Association lists: By creating a list with a hey / value structure -
		ex: (key (value)) - (blt (bacon lettuce tomato)) - etc. 
	We create what is essentially a map in Java. By calling the key value, it will return the value in the following parentheses.

	In lisp these are referred to Association Lists or alis 

	When possible, working with display agnostic 
	data types for text is preferred to working with strings. For example storing text in lists or as variables of data, as opposed to merely string types, can lead to more elegant and extensible code later. 

	Assoc function
	(assoc key aList)
	Assoc will return the related value to the key in the aList. If no key exists, assoc will return nil. 

	In our version of the method for describing the location, we pass in the list of nodes. We do this to adhere to the tennets of functional programming.
	In functional programming, a function may only operate on parameters, or variables described in the function itself.

	Higher Order Functions: Functions which take other functions as a parameter.
		when working with higher order, and passing another function in to a function, it is pre-empted with an #, which is short for function. 

	Quasiquoting-
		Quasiquoting is a way to allow a statement to switch between data and code within itself.
		To begin quasiquoting a data statement must begin with a back quote (`) instead of a regular quote (')
		After that any subsequent parentheses prefaced by a comma (,) will be treated as code, and not data. 

	A look at lines 15-16 - 
		Working backwards: (cdr(assoc location edges))
			The assoc pulls out the list value from edges related to the key value location. In the code above that gives us a list containing (location direction exit-type)
			The cdr returns us the back of the list. In this case it gives us back the direction the exit heads, and the type of exit.

		(mapcar #'describe-path (cdr (assoc location edges)))
			Mapcar is a function which takes in a function, and a list, and then applies the function to each item in the list. 

			functions such as mapcar, which take other functions as parameters are called 'higher order functions'

		(apply #'append (mapcar #'describe-paht (cdr (assoc location edges))))
			Append is a fucntion which combines multiple lists into a single list. In this function we are using it to combine edges and descriptions of said paths. 

	Review: 
		Labels - this is a function that allows us to declare a function locally, within another context. 

	Lisp convention: 
		When declaring a function that has the possibilty of returning either nil or a truth value, it is convention to apply a '-p' to the end of the function name. This is because methods which return a nil or truth value in Lisp are called predicates. 

	Keyword parameters
		Keyword parameters are composed of 2 main parts
			First: Name. In the code we wrote we used the keyword parameter :key 
			Second: Value. In the code we wrote the value was #'cadr

	Attempted outside of guide: 
		Drop
		(drop object) will leave the object in the room you're currently in


	CONCEPTS:
		Assoc 
		ALists
		Quasiquoting
		Higher order functions
		mapCar
		