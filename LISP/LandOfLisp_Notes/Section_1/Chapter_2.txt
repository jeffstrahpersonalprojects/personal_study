Creating Your First Lisp Program

This chapter walks the reader through creating their first 2 functioning programs. This includes a basic Hello World, as well as a Number Guesser game, which covers some new and truly interesting concepts to someone new to functional programming. 

Concepts covered: 
	Calling a function: In Lisp you call a function by putting in a set of parentheses, along with any parameters the function requires. 

	Binary search: Not just a Lisp concept, but a basic programming tool for searching through a large number of sorted values quickly. This is done by continually eliminating half of the remaining possibilities until arrival at the correct value. 

	Defining global variables in Lisp: Here we learn function defparameter, which defines a global variable in a Lisp program (Also called a	top-level definition). In this syntax we learn defparameter which takes in two parameters, a name for the variable, and the initial value. The book also lists as a common practice, wrapping the names of class level variables in asterisks, or 'ear muffs'. This is a way to distinguish them for variables declared in other contexts.

	Defining global variables without overwriting: Another option for declaring a global variable is defVar, which has the same parameters, but will not overwrite existing values. 

	Defining global functions: The function defun operates similar to defparameter, in that it takes in a name, and other values, however for a funciton those values go : Name, parameters(enclosed in parentheses), the function itself. If a function has no parameters the function just takes an empty set of parentheses (). 

	The Ash function: This was an amazingly fun and broadening function to learn about. With Ash we take a number, then we either double or half it in what is possibly the most interesting way. We move the final digit of the binary value one space to the left or right, which will either double or half the number (Assuming the final binary digit is a 0. Otherwise it's not an even number and it becomes half -.5. Also if the function adds digits to the left it adds a zero, making it always even) You can continue to do this by moving the final digit further to the left or right. Coming from a Java background I've never manipulated a binary value before, and to me this was a mind blowing experience. Truly this simple function is one of the simplest examples of arcane wizardry in programming. 

	Returning values from functions: In Lisp this happens automatically. All functions return the final calculated value automatically without a return statement.

	Setting global variables: the function setf will set a variable to a desired value. This function takes in the name of the variable being reassigned, and the new value as parameters.

	Local variables in Lisp: Local variables only apply to that specific function, or block of code. The function for declaring a local variable is (let (declarations) -code-goes-here- ). Where the variables declared in the declarations can be accessed by the code block within the parentheses. You can declare multiple variables at a time, encasing each in it's own set of parentheses.

	Local functions in Lisp: Like local variables, local functions only apply to specific blocks of code. The function for declaring a local function is (fLet ((Name (Parameters) -function-itself- )) -code-goes-here) Similarly you can declare multiple functions with one flet, encasing each in it's own set of parentheses.

New Information:
	Local Variable
	Global Variable
	Global Function
	Local Function
	Ash Function
	Return Values

